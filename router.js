import express from 'express';
import { v4 as uuidv4 } from 'uuid';
import ms from 'ms';
import db from './db.js';
import sessions from './sessions.js';

const router = express.Router();

router.get('/', (req, res) => {
    res.redirect('/login');
});

router.get('/login', (req, res) => {
    if (sessions[req.cookies.session]) {
        return res.redirect('/main');
    }

    res.render('login', {
        error: null,
        ...req.query,
    });
});

router.post('/login', (req, res) => {
    db.get(`SELECT * FROM users WHERE login='${req.body.login}' AND password='${req.body.password}'`, (err, result) => {
        if (err) {
            res.redirect(`/login?error=${err.message}`);
            return;
        }
        if (!result) {
            res.redirect(`/login?error=Invalid username or password. Please try again`);
            return;
        }
        const sessionId = uuidv4();
        sessions[sessionId] = req.body.login;
        res.cookie('session', sessionId, {
            maxAge: ms('365 days'),
        });
        res.redirect('/main');
    });
});

router.get('/register', (req, res) => {
    if (sessions[req.cookies.session]) {
        return res.redirect('/main');
    }

    res.render('register', {
        message: null,
        ...req.query,
    });
});

router.post('/register', (req, res) => {
    db.get(`SELECT * FROM users WHERE login='${req.body.login}'`, (err, result) => {
        if (err) {
            res.redirect(`/register?message=${err.message}`);
            return;
        }
        if (result) {
            res.redirect(`/register?message=This name is already taken. Please choose another name.`);
            return;
        }
        db.run(`INSERT INTO users VALUES ('${req.body.login}', '${req.body.password}')`, (err) => {
            if (err) {
                res.redirect(`/register?message=${err.message}`);
                return;
            }
            res.redirect(`/register?success=true`);
        });
    });
});

router.get('/main', (req, res) => {
    if (!sessions[req.cookies.session]) {
        return res.redirect('/login');
    }

    db.all(`SELECT * FROM comments`, (err, comments) => {
        res.render('main', {
            comments: comments || [],
            author: sessions[req.cookies.session],
        });
    });
});

router.post('/comment', (req, res) => {
    db.run(`INSERT INTO comments VALUES ('${req.body.content}', '${sessions[req.cookies.session]}')`, (err) => {
        res.redirect('/main');
    });
});

router.get('/logout', (req, res) => {
    delete sessions[req.cookies.session];
    res.cookie('session', req.cookies.session, {
        expires: 0,
    });
    return res.redirect('/login');
});

export default router;